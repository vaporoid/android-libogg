LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE := ogg
LOCAL_C_INCLUDES := \
	../include
LOCAL_SRC_FILES := \
	../src/bitwise.c \
	../src/framing.c

LOCAL_ARM_MODE := arm

include $(BUILD_SHARED_LIBRARY)
#include $(BUILD_STATIC_LIBRARY)
